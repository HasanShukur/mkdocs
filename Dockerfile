FROM python:3.11
RUN pip install mkdocs mkdocs-material mkdocs-pymdownx-material-extras mkdocs-minify-plugin markdown-include mkdocs-include-markdown-plugin mkdocs-awesome-pages-plugin
WORKDIR /app
COPY . .
EXPOSE 8000
CMD ["mkdocs", "serve", "--dev-addr=0.0.0.0:8000"]