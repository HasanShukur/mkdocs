# [Python API Development - Comprehensive Course for Beginners](https://www.youtube.com/watch?v=0sOvCWFmrtA)

## Documentation for Project Notes

1. Create the following directory 

    ```console
    mkdir mkdocs.Dockerfile
    ```

2. Create a **Dockerfile** in this directory

    ```console
    cd mkdocs.Dockerfile
    echo 'FROM squidfunk/mkdocs-material' > Dockerfile 
    echo 'RUN pip install mkdocs-include-markdown-plugin' >> Dockerfile 
    echo 'RUN pip install mkdocs-awesome-pages-plugin' >> Dockerfile
    ```

3. Run the following command to create a new mkdocs site.  

    ```console
    docker run --rm -it -v ${PWD}:/docs squidfunk/mkdocs-material new .
    ```

4. If you are using vscode change the user settings for material for mkdocs

    ```json
    "yaml.schemas": {
        "https://squidfunk.github.io/mkdocs-material/schema.json": "mkdocs.yml"
    },
    "yaml.customTags": [
        "!ENV scalar",
        "!ENV sequence",
        "tag:yaml.org,2002:python/name:materialx.emoji.to_svg",
        "tag:yaml.org,2002:python/name:materialx.emoji.twemoji",
        "tag:yaml.org,2002:python/name:pymdownx.superfences.fence_code_format"
    ],
    ```

5. Create a **docker-compose.yml** file

    ```yaml
    version: "3.7"

    services:
    docs:
        build: ./mkdocs.Dockerfile
        ports:
        - 8008:8000
        volumes:
        - ./:/docs
    ```

6. Run the container serving the the Documentation

    ```console
    docker compose up -d
    ```



