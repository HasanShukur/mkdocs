import sys
import requests
import os

def update_variable(api_url, access_token, variable_name, new_value):

    print ("here for rest put")
    url = f"{api_url}/variables/{variable_name}?private_token={access_token}"
    payload={'value': new_value}

    response = requests.put(url,data=payload)
    print ("url is ", url)
    if response.status_code == 200:
        print(f"Variable '{variable_name}' updated successfully.")
    else:
        print(f"Error updating variable '{variable_name}': {response.status_code} - {response.text}")


def update_version(old_version, labels):
    version_parts = [int(part) for part in old_version.split('.')]  # Learn version parts from old version

    if "major" in labels:
        version_parts[0] += 1  # Increment major version

    if "minor" in labels:
        version_parts[1] += 1  # Increment minor version

    if "patch" in labels:
        version_parts[2] += 1  # Increment bugfix version

    new_version = ".".join(str(part) for part in version_parts)

    return new_version

if __name__ == "__main__":
    if len(sys.argv) > 3:
        old_version = sys.argv[2]
        if old_version:
            labels_str = sys.argv[1]
            labels = labels_str.split(',')
            labels = [label.strip() for label in labels]

            new_version = update_version(old_version, labels)
            print("got version", old_version , " and ", new_version)
            api_url = "https://gitlab.com/api/v4/projects/46537885"
            access_token = sys.argv[3]
            variable_name = "VERSIONING"
            print ("Token is", access_token)
            update_variable(api_url, access_token, variable_name, new_version)
            os.environ["UPDATED_VERSION"] = new_version

            print(new_version)

            with open("token.txt", "w") as file:
                file.write(access_token)
                file.close

    else:
        print("ERROR")
