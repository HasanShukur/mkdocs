import os
import pytest
from selenium import webdriver

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')

driver = webdriver.Chrome(options=chrome_options)

test_url = os.environ.get('TEST_URL')
assertion_value = os.environ.get('ASSERTION_VALUE')

@pytest.fixture(scope="module")
def browser():
    driver = webdriver.Chrome(options=chrome_options)
    yield driver
    driver.quit()

@pytest.mark.slow
def test_selenium_title(browser):
    browser.get(test_url)
    title = browser.title
    print("Page title:", title)
    assert assertion_value in title

@pytest.mark.slow
def test_selenium_page_size(browser):
    browser.get(test_url)
    page_width = browser.execute_script("return document.documentElement.clientWidth")
    page_height = browser.execute_script("return document.documentElement.clientHeight")
    print("Page size:", page_width, "x", page_height)

# Close the browser (if needed)
driver.quit()