import os
import ast

def analyze_folder(folder_path):
    for root, _, files in os.walk(folder_path):
        for file_name in files:
            if file_name.endswith('.py'):
                file_path = os.path.join(root, file_name)
                print(file_path)
                analyze_file(file_path)

def analyze_file(file_path):
    with open(file_path, 'r') as file:
        try:
            ast_tree = ast.parse(file.read())
            analyze_ast_tree(ast_tree)
        except SyntaxError as e:
            print(f"Syntax error in file {file_path}: {e}")

def analyze_ast_tree(ast_tree):
    # Perform your analysis on the AST tree here
    # You can access different nodes and analyze their properties

    for node in ast.walk(ast_tree):
        if isinstance(node, ast.FunctionDef):
            print(f"Found function: {node.name}")
        elif isinstance(node, ast.ClassDef):
            print(f"Found class: {node.name}")
        # Add more conditions to analyze other AST node types

# Example usage: analyzing a folder named 'my_project'
folder_path = './'
analyze_folder(folder_path)

