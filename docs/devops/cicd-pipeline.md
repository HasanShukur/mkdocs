---
title: CI/CD-Pipeline
---

A CI/CD (Continuous Integration/Continuous Deployment) pipeline typically consists of several stages that help automate the software development and deployment process. 

```
stages:
  - build
  - test
  - versioning
  - argocdchanges
  - tagging
  - deploy

variables:
  OLD_VERSION: $VERSIONING
  SESSION_TOKEN: "glpat-c8rDeUhisHgFw4MxpTLz"
  BUILD_SUCCEEDED_CONTAINER_ID: ""

build-image:
  stage: build
  before_script:
    - docker info
  tags:
    - mkdocs
  script:
    - |
      container_name="bntprodevops/mkdocs"
      RUNNING_IDS=$(docker ps -a --filter "name=mkdocs_bntpro" --format "{{.ID}}")
      if [ -n "$RUNNING_IDS" ]; then
        echo "${RUNNING_IDS[@]}"
        docker stop "$RUNNING_IDS"
        docker rm "$RUNNING_IDS"
      else
        echo "RUNNING_IDS is empty."
      fi
      docker images --filter=reference="bntprodevops/mkdocs" --format "{{.ID}}" | while read -r image_id; do if ! docker ps -q --filter="ancestor=$image_id" | grep -q .; then docker rmi "$image_id"; fi; done
      docker build -t "bntprodevops/mkdocs:latest" .
  only:
    - merge_requests

test:
  stage: test
  tags:
    - mkdocs
  script:
    - |
      pwd
      container_name="bntprodevops/mkdocs"
      RUNNING_IDS=$(docker ps -a --filter "name=mkdocs_bntpro" --format "{{.ID}}")
      if [ -n "$RUNNING_IDS" ]; then
        echo "${RUNNING_IDS[@]}"
        docker stop "$RUNNING_IDS"
        docker rm "$RUNNING_IDS"
      else
        echo "RUNNING_IDS is empty."
      fi

      docker run -d -p 8111:8000 --name "mkdocs_bntpro" "$container_name:latest"
      sleep 30
      docker images --filter=reference="bntprodevops/mkdocs" --format "{{.ID}}" | while read -r image_id; do if ! docker ps -q --filter="ancestor=$image_id" | grep -q .; then docker rmi "$image_id"; fi; done
      docker build -t selenium-test test/.
      docker run --rm -e TEST_URL="http://mkdocs_bntpro:8000" -e ASSERTION_VALUE="Material for MkDocs - Material for MkDocs" -v "$(pwd)/test/reports:/app/reports" --name selo_test --link mkdocs_bntpro selenium-test
  artifacts:
    when: always
    paths:
      - test/reports/rspec.xml
    reports:
      junit: test/reports/rspec.xml
  only:
    - merge_requests

versioning-image:
  stage: versioning
  tags:
    - mkdocs
  variables:
    NEW_VERSION: $VERSIONING
  cache:
    untracked: true
  script:
    - python versioning_script.py ${CI_MERGE_REQUEST_LABELS} ${OLD_VERSION} ${SESSION_TOKEN}
    - echo ${CI_MERGE_REQUEST_LABELS}
    - echo "will be commit to new tag"
  only:
    - merge_requests

argocd-changes:
  stage: argocdchanges
  tags:
    - mkdocs
  variables:
    NEW_VERSION: $VERSIONING
  script:
    - echo "${VERSIONING}"
    - sed -i "s/##MY_VERSION##/${VERSIONING}/g" ./Argocd/deployment.yaml 
  only:
    - merge_requests

tagging-image:
  stage: tagging
  tags:
    - mkdocs
  script:
    - BUILD_SUCCEEDED_CONTAINER_ID=$(docker ps --filter "ancestor=bntprodevops/mkdocs:latest" --format "{{.ID}}")
    - echo "${BUILD_SUCCEEDED_CONTAINER_ID}"
    - printenv
    - echo "versioning is ${VERSIONING}"
    - docker commit "$BUILD_SUCCEEDED_CONTAINER_ID" "bntprodevops/mkdocs:$VERSIONING"
    - echo "committed to new tag bntprodevops/mkdocs:${VERSIONING}"   
  only:
    - merge_requests

deploy-image:
  stage: deploy
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD"
  tags:
    - mkdocs
  variables:
    NEW_VERSION: $VERSIONING  
  script:
    - echo  ${NEW_VERSION}
    - docker push "bntprodevops/mkdocs:$NEW_VERSION"
  only:
    - merge_requests


```
### Here's a description of each stage:

##Build: 
 In this stage, the source code of the application is compiled and transformed into an executable or deployable format. It involves tasks like compiling code, resolving dependencies, and generating artifacts.

##Test:
 The test stage is crucial for ensuring the quality of the software. Various types of tests, such as unit tests, integration tests, and functional tests, are executed to verify that the application behaves as expected and meets the defined requirements.

##Versioning: 
 Versioning involves assigning a unique identifier to the software release or artifact. It helps track and manage different versions of the application, enabling easier identification, retrieval, and rollback if necessary.

##Argocdchanges: 
 This stage seems specific to Argocd, a popular GitOps continuous delivery tool. In this stage, any changes made to the application's configuration or deployment manifests are identified and prepared for deployment. It may involve reviewing and validating the changes and ensuring they align with the desired state.

##Tagging:
 Tagging refers to attaching a label or tag to a specific version or release of the software. Tags are typically used for tracking and organizing different versions, allowing for easy identification and retrieval during deployment or rollback processes.

##Deploy:
 The deployment stage involves pushing the built and tested artifacts to the target environment, such as staging or production. It can include tasks like provisioning infrastructure, configuring services, and deploying the application code. The deployment can be done manually or in an automated manner, depending on the pipeline setup.

Overall, these stages collectively automate the process of building, testing, versioning, making changes, tagging, and deploying software, ensuring a consistent and reliable software delivery process.
