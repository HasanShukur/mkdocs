---
title: Make Own Pages
---

## Creating and Organizing Pages

Before making our web page, we should first understand our site directory tree. Let's explore the base image directory tree below:
```
├── docs
│   ├── Home
│   │   ├── index.md
│   ├── getting-start/
│   │   ├──login-to-registry.md
│   │   ├──get-base-image.md
│   │   ├──make-own-pages.md
│   │   ├──awesome-pages.md
│   │   ├──run-on-docker.md
│   ├── mkdocs-configuration/
│   │   ├── ... # configuration documents.
│   ├── devops/
│   │   ├── cicd-flow.md
│   │   ├── cicd-pipeline.md
│   ├── assets/
│   │   ├──images/
│   │   ├──screenshots/
│   │   ├──favicon.png
│   ├── ...
``` 
To make our own web pages we must to create markdown files and subfolders in the "/docs" directory. You may follow below steps to create your own basic web pages under tutorial direcotry.
To create a tutorial directory and several Markdown files under the /docs directory in your MkDocs project, follow these steps:
1. Navigate to your MkDocs project directory:
```
cd /path/to/your/mkdocs/docs
```
2. Create a new directory named tutorial under the /docs directory:
```
mkdir tutorial
```
3. Change into the newly created tutorial directory:
```
cd docs/tutorial
``` 
4.Create multiple Markdown files for your tutorial. You can use any text editor to create and edit these files. For example, using the command line, you can create three Markdown files named step1.md, step2.md, and step3.md:
```
touch step1.md
touch step2.md
touch step3.md
```
5. Edit the Markdown files with the content you want. For instance, open each file in a text editor and add some content. For example, in step1.md, you can add:
```
---
title: tutorial first page
---

# Step 1

This is the content of Step 1 in the tutorial.
```
And in step2.md, you can add:
```
---
title: tutorial second page
---
# Step 2

This is the content of Step 2 in the tutorial.
```
Similarly, add content to step3.md.

6. Go back to the root of your MkDocs project directory:
```
cd ../..
```
7. Update the mkdocs.yml configuration file to include the tutorial directory in the navigation. Open mkdocs.yml in a text editor and add the following lines under the nav section:
```
nav:
    - Home: index.md
    - Tutorial:
        - Step 1: docs/tutorial/step1.md
        - Step 2: docs/tutorial/step2.md
        - Step 3: docs/tutorial/step3.md
```
Note that the indentation is crucial in YAML, and it represents the nested structure of the navigation.

8. Save the changes to mkdocs.yml.

## Adding pictures
To add a picture from the /assets/images directory to your Markdown files and provide details for the image, follow these steps:
1. Place the image in the /assets/images directory:
Copy the image you want to include in your tutorial and paste it into the /assets/images directory in your MkDocs project.

2. Update the Markdown files with the image and details:
Open each Markdown file (e.g., step1.md, step2.md, etc.) in a text editor and add the following Markdown syntax to include the image and provide details:
```
# Step 1

This is the content of Step 1 in the tutorial.

![Image Description](../assets/images/image_filename.png)

More content here...
```
Replace image_filename.png with the actual filename of the image you placed in the /assets/images directory. The ![Image Description](image_path) syntax adds the image to the page, and the Image Description is the alternative text for the image, which is displayed when the image cannot be loaded.

3. Repeat the same steps for other Markdown files in the tutorial.

4. Save the changes to the Markdown files.

5. Go back to the root of your MkDocs project directory:
```
cd /path/to/your/mkdocs/
```
6. Preview your documentation by running the development server:
```
docker-compose build
docker-compose up
```