To log in to Docker Hub from the command line, you can use the docker login command followed by your Docker Hub username and password. Here are the steps to log in to Docker Hub:

1. Open a terminal or command prompt.

2. Run the following command:
```
docker login
```
This command initiates the login process.

3. You will be prompted to enter your Docker Hub username. Type in your Docker Hub username and press Enter.

4. Next, you will be prompted to enter your Docker Hub password. Type in your Docker Hub password (note that the password won't be visible as you type) and press Enter.

5. If the provided credentials are correct, Docker will authenticate you and display a success message.

Once you have logged in to Docker Hub, you can perform actions such as pushing your local images to Docker Hub or pulling images from Docker Hub to your local machine.
