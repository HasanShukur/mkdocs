Step 1. Install Docker Desktop

Download and install Docker Desktop from the official Docker website for your operating system (Windows or macOS).
Follow the installation instructions provided by Docker.

Step 2. Create a project folder

Create a new folder on your local machine where you want to set up your MkDocs project.

Step 3. Download base files

Open a terminal or command prompt and navigate to the project folder you created in Step 2.
Run the following command to download the base files for MkDocs:

```
curl -LO https://gitlab.com/bntprodevops/docmat/-/archive/main/docmat-main.zip 
```

Step 4. Extract the downloaded files

Extract the contents of the downloaded ZIP file to the project folder. You can use any suitable tool for extraction, such as the built-in extraction functionality of your operating system or a third-party tool like 7-Zip.

Step 5. Configure your MkDocs project

Open the project folder in a text editor.
Modify the mkdocs.yml file to configure your project settings, such as the site name, theme, and pages structure.
Add your documentation content by creating Markdown files in the project folder or its subdirectories.

Step 6. Run Docker Compose

Open a terminal or command prompt on your machine. Navigate to the project folder where the docker-compose.yml file is located. Use the cd command followed by the folder path to change directories.

```
cd docmat-main
```
Run the following command to start the MkDocs server using Docker Compose:

```
docker-compose up 
```

Docker Compose will pull the MkDocs Docker image if it's not already available on your system. It will then start the MkDocs server and bind it to port 8000 on your local machine.

Step 7. Access your MkDocs site

Once the Docker Compose command has executed successfully, open a web browser.
Visit http://localhost:8000 in the browser.
You should now see your MkDocs site with your documentation content.
As you make changes to your MkDocs project files (e.g., mkdocs.yml, Markdown files), the site will be automatically updated when you refresh the page in your browser.


That's it! You have now successfully downloaded the base files for MkDocs, configured your MkDocs project, and set up Docker Compose to run MkDocs using Docker Desktop.
